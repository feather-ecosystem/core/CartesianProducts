# CartesianProducts

[![pipeline status](https://gitlab.com/feather-ecosystem/CartesianProducts/badges/master/pipeline.svg)](https://gitlab.com/feather-ecosystem/CartesianProducts/-/commits/master)
[![coverage report](https://gitlab.com/feather-ecosystem/CartesianProducts/badges/master/coverage.svg)](https://gitlab.com/feather-ecosystem/CartesianProducts/-/commits/master)
[![codecov](https://codecov.io/gl/feather-ecosystem:core/CartesianProducts/branch/master/graph/badge.svg?token=CADX5A60R2)](https://codecov.io/gl/feather-ecosystem:core/CartesianProducts)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://feather-ecosystem.gitlab.io/core/CartesianProducts)
