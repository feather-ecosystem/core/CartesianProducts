using CartesianProducts, FeaInterfaces

package_info = Dict(
    "modules" => [CartesianProducts],
    "authors" => "Rene Hiemstra and contributors",
    "name" => "CartesianProducts.jl",
    "repo" => "https://gitlab.com/feather-ecosystem/core/CartesianProducts",
    "pages" => [
        "About"  =>  "index.md"
        "API"  =>  "api.md"
    ],
)

DocMeta.setdocmeta!(CartesianProducts, :DocTestSetup, :(using CartesianProducts); recursive=true)

# if docs are built for deployment, fix the doctests
# which fail due to round off errors...
if haskey(ENV, "DOC_TEST_DEPLOY") && ENV["DOC_TEST_DEPLOY"] == "yes"
    doctest(CartesianProducts, fix=true)
end
