# CartesianProducts.jl
[![pipeline status](https://gitlab.com/feather-ecosystem/CartesianProducts/badges/master/pipeline.svg)](https://gitlab.com/feather-ecosystem/CartesianProducts/-/commits/master)
[![coverage report](https://gitlab.com/feather-ecosystem/CartesianProducts/badges/master/coverage.svg)](https://gitlab.com/feather-ecosystem/CartesianProducts/-/commits/master)
[![codecov](https://codecov.io/gl/feather-ecosystem:core/CartesianProducts/branch/master/graph/badge.svg?token=CADX5A60R2)](https://codecov.io/gl/feather-ecosystem:core/CartesianProducts)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://feather-ecosystem.gitlab.io/core/CartesianProducts)

***

This package implements Cartesian products of iterable sets.

!!! tip
    This package is a part of the [`Feather`](https://gitlab.com/feather-ecosystem) project. It is tightly integrated into the ecosystem of packages provided by Feather. If you are interested in applications of the functionality implemented in this package, please visit the main documentation of the [`ecosystem`](https://feather-ecosystem.gitlab.io/feather/).